package com.aigestudio.downloader.bizs;

import ohos.app.Context;
import ohos.data.rdb.RdbStore;
import ohos.data.resultset.ResultSet;

import java.util.ArrayList;
import java.util.List;

import static com.aigestudio.downloader.bizs.DLCons.DBCons.TB_THREAD;
import static com.aigestudio.downloader.bizs.DLCons.DBCons.TB_THREAD_END;
import static com.aigestudio.downloader.bizs.DLCons.DBCons.TB_THREAD_ID;
import static com.aigestudio.downloader.bizs.DLCons.DBCons.TB_THREAD_START;
import static com.aigestudio.downloader.bizs.DLCons.DBCons.TB_THREAD_URL_BASE;

class ThreadDAO implements IThreadDAO {
    private final DLDBHelper dbHelper;

    ThreadDAO(Context context) {
        dbHelper = new DLDBHelper(context);
    }

    @Override
    public void insertThreadInfo(DLThreadInfo info) {
        RdbStore db = dbHelper.getRdbStore();
        db.executeSql("INSERT INTO " + TB_THREAD + "(" +
                        TB_THREAD_URL_BASE + ", " +
                        TB_THREAD_START + ", " +
                        TB_THREAD_END + ", " +
                        TB_THREAD_ID + ") VALUES (?,?,?,?)",
                new Object[]{info.baseUrl, info.start, info.end, info.id});
    }

    @Override
    public void deleteThreadInfo(String id) {
        RdbStore db = dbHelper.getRdbStore();
        db.executeSql("DELETE FROM " + TB_THREAD + " WHERE " + TB_THREAD_ID + "=?", new String[]{id});
    }

    @Override
    public void deleteAllThreadInfo(String url) {
        RdbStore db = dbHelper.getRdbStore();
        db.executeSql("DELETE FROM " + TB_THREAD + " WHERE " + TB_THREAD_URL_BASE + "=?",
                new String[]{url});
    }

    @Override
    public void updateThreadInfo(DLThreadInfo info) {
        RdbStore db = dbHelper.getRdbStore();
        db.executeSql("UPDATE " + TB_THREAD + " SET " +
                TB_THREAD_START + "=? WHERE " +
                TB_THREAD_URL_BASE + "=? AND " +
                TB_THREAD_ID + "=?", new Object[]{info.start, info.baseUrl, info.id});
    }

    @Override
    public DLThreadInfo queryThreadInfo(String id) {
        DLThreadInfo info = null;
        RdbStore db = dbHelper.getRdbStore();
        ResultSet c = db.querySql("SELECT " +
                TB_THREAD_URL_BASE + ", " +
                TB_THREAD_START + ", " +
                TB_THREAD_END + " FROM " +
                TB_THREAD + " WHERE " +
                TB_THREAD_ID + "=?", new String[]{id});
        if (c.goToFirstRow()) info = new DLThreadInfo(id, c.getString(0), c.getInt(1), c.getInt(2));
        c.close();
        return info;
    }

    @Override
    public List<DLThreadInfo> queryAllThreadInfo(String url) {
        List<DLThreadInfo> info = new ArrayList<>();
        RdbStore db = dbHelper.getRdbStore();
        ResultSet c = db.querySql("SELECT " +
                TB_THREAD_URL_BASE + ", " +
                TB_THREAD_START + ", " +
                TB_THREAD_END + ", " +
                TB_THREAD_ID + " FROM " +
                TB_THREAD + " WHERE " +
                TB_THREAD_URL_BASE + "=?", new String[]{url});
        while (c.goToNextRow())
            info.add(new DLThreadInfo(c.getString(3), c.getString(0), c.getInt(1), c.getInt(2)));
        c.close();
        return info;
    }
}