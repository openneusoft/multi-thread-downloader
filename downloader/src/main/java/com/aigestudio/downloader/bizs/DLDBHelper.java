package com.aigestudio.downloader.bizs;

import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.rdb.RdbException;
import ohos.data.rdb.RdbOpenCallback;
import ohos.data.rdb.RdbStore;
import ohos.data.rdb.StoreConfig;

import static com.aigestudio.downloader.bizs.DLCons.DBCons.TB_TASK_SQL_CREATE;
import static com.aigestudio.downloader.bizs.DLCons.DBCons.TB_TASK_SQL_UPGRADE;
import static com.aigestudio.downloader.bizs.DLCons.DBCons.TB_THREAD_SQL_CREATE;
import static com.aigestudio.downloader.bizs.DLCons.DBCons.TB_THREAD_SQL_UPGRADE;

final class DLDBHelper extends RdbOpenCallback {
    private static final String DB_NAME = "dl.db";
    private static final int DB_VERSION = 3;
    private static RdbStore database;
    private static DatabaseHelper helper;

    DLDBHelper(Context context) {
        super();
        if(database == null){
            helper = new DatabaseHelper(context);
            StoreConfig storeConfig = StoreConfig.newDefaultConfig(DB_NAME);
            database = helper.getRdbStore(storeConfig, DB_VERSION, this, null);
        }else {
            if (database.isOpen() && database.isReadOnly()) {
                throw new RdbException("Can't upgrade write database from version " +
                        database.getVersion() + " to " + DB_VERSION + ": " + DB_NAME);
            }
        }
    }

    @Override
    public void onCreate(RdbStore db) {
        db.executeSql(TB_TASK_SQL_CREATE);
        db.executeSql(TB_THREAD_SQL_CREATE);
    }

    @Override
    public void onUpgrade(RdbStore db, int oldVersion, int newVersion) {
        db.executeSql(TB_TASK_SQL_UPGRADE);
        db.executeSql(TB_THREAD_SQL_UPGRADE);
        onCreate(db);
    }

    public RdbStore getRdbStore() {
        return this.database;
    }
}