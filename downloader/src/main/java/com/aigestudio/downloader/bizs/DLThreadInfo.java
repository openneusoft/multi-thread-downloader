package com.aigestudio.downloader.bizs;

import java.io.Serializable;

public class DLThreadInfo implements Serializable {
    String id;
    String baseUrl;
    int start, end;
    boolean isStop;

    DLThreadInfo(String id, String baseUrl, int start, int end) {
        this.id = id;
        this.baseUrl = baseUrl;
        this.start = start;
        this.end = end;
    }
}