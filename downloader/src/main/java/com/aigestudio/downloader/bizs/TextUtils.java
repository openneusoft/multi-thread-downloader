package com.aigestudio.downloader.bizs;

import java.text.DecimalFormat;

public class TextUtils {
    public static boolean isEmpty(String value) {
        if(value == null || "".equals(value)) {
            return true;
        }
        return false;
    }

    public static String converFileSize(long size) {
        String result = "";
        long kb = 1024;
        long mb = kb * 1024;
        long gb = mb * 1024;

        DecimalFormat df = new DecimalFormat("#.##");

        if (size >= gb) {
            result = df.format((double) size / gb) + "GB";
        } else if (size >= mb) {
            result = df.format((double) size / mb) + "MB";
        } else if (size >= kb) {
            result = df.format((double) size / mb) + "KB";
        } else {
            result = size + "B";
        }
        return result;
    }
}
