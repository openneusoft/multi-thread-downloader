package com.aigestudio.downloader.interfaces;

import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.utils.PacMap;

import java.io.File;
import java.lang.ref.SoftReference;

/**
 * Created by yjwfn on 16-3-2.
 */
public final class AsyncDListener implements IDListener {

    private static final String EXTRA_ARGS_1 = "extra_args_1";
    private static final String EXTRA_ARGS_2 = "extra_args_2";
    private static final String EXTRA_ARGS_3 = "extra_args_3";

    private static final int OP_PREPARE =   0x0;
    private static final int OP_START   =   0x1;
    private static final int OP_PROGRESS =  0x2;
    private static final int OP_STOP    =   0x3;
    private static final int OP_FINISH  =   0x4;
    private static final int OP_ERROR   =   0x5;
    private static final int OP_TASKWAIT   =   0x6;

    private final SoftReference<IDListener>   mRef;


    private static final SoftReference<EventHandler>   mHandlerRef;

    static {
        EventRunner runner = EventRunner.create("downloadRunner");
        mHandlerRef = new SoftReference<EventHandler>(new EventHandler(runner){
            @Override
            public void processEvent(InnerEvent msg) {

                @SuppressWarnings("unchecked")
                SoftReference<IDListener> ref = (SoftReference<IDListener>) msg.object;
                IDListener realListener = ref.get();

                if(realListener == null)
                    return;

                PacMap bundle = msg.getPacMap();
                switch (msg.eventId){
                    case OP_PREPARE:
                        realListener.onPrepare();
                        break;
                    case OP_START:
                        realListener.onStart(bundle.getString(EXTRA_ARGS_1),
                                bundle.getString(EXTRA_ARGS_2),
                                bundle.getIntValue(EXTRA_ARGS_3));
                        break;
                    case OP_STOP:
                        realListener.onStop(bundle.getIntValue(EXTRA_ARGS_1));
                        break;
                    case OP_PROGRESS:
                        realListener.onProgress(bundle.getIntValue(EXTRA_ARGS_1));
                        break;
                    case OP_FINISH:
                        File file = (File)bundle.getSerializable(EXTRA_ARGS_1).get();
                        realListener.onFinish(file);
                        break;
                    case OP_ERROR:
                        realListener.onError(bundle.getIntValue(EXTRA_ARGS_1),
                                bundle.getString(EXTRA_ARGS_3) );
                        break;
                    case OP_TASKWAIT:
                        realListener.onWait(bundle.getIntValue(EXTRA_ARGS_1));
                        break;

                }
            }
        });
    }

    public AsyncDListener(IDListener listener){
        mRef = new SoftReference<>(listener);
    }


    @Override
    public void onPrepare() {
        invokeOnMainThread(OP_START, null);
    }

    @Override
    public void onStart(String fileName, String realUrl, int fileLength) {
        PacMap bundle = new PacMap();
        bundle.putString(EXTRA_ARGS_1, fileName);
        bundle.putString(EXTRA_ARGS_2, realUrl);
        bundle.putIntValue(EXTRA_ARGS_3, fileLength);
        invokeOnMainThread(OP_START, bundle);
    }

    @Override
    public void onProgress(int progress) {
        PacMap bundle = new PacMap();
        bundle.putIntValue(EXTRA_ARGS_1, progress);
        invokeOnMainThread(OP_PROGRESS, bundle);
    }

    @Override
    public void onStop(int progress) {
        PacMap bundle = new PacMap();
        bundle.putIntValue(EXTRA_ARGS_1, progress);
        invokeOnMainThread(OP_STOP, bundle);
    }

    @Override
    public void onFinish(File file) {
        PacMap bundle = new PacMap();
        bundle.putSerializableObject(EXTRA_ARGS_1, file);
        invokeOnMainThread(OP_FINISH, bundle);
    }

    @Override
    public void onError(int status, String error) {
        PacMap bundle = new PacMap();
        bundle.putIntValue(EXTRA_ARGS_1, status);
        bundle.putString(EXTRA_ARGS_2, error);
        invokeOnMainThread(OP_ERROR, bundle);
    }

    @Override
    public void onWait(int taskSize) {
        PacMap bundle = new PacMap();
        bundle.putIntValue(EXTRA_ARGS_1, taskSize);
        invokeOnMainThread(OP_PROGRESS, bundle);
    }


    private void invokeOnMainThread(int what, PacMap data){
        if(mHandlerRef.get() != null) {
            EventHandler handler = mHandlerRef.get();
            InnerEvent  message = InnerEvent.get(what, what, mRef);
            message.setPacMap(data);
            handler.sendEvent(message);
        }
    }


    public static IDListener  wrap(IDListener listener){
        return listener != null ? new AsyncDListener(listener) : null;
    }
}
