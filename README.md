# multi-thread-downloader

**本项目是基于开源项目MultiThreadDownloader进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/AigeStudio/MultiThreadDownloader ）追踪到原项目版本**

#### 项目介绍

- 项目名称：多线程下载器
- 所属系列：ohos的第三方组件适配移植
- 功能：轻量级支持断点续传的多线程下载器。
- 项目移植状态：完成
- 调用差异：无
- 原项目Doc地址：https://github.com/AigeStudio/MultiThreadDownloader
- 原项目基线版本：v1.0 , sha1:8bad57fad22c4860c8eece721ff3fbbde83c7fed
- 编程语言：Java 
- 外部库依赖：无

#### 效果展示

<img src="https://gitee.com/openneusoft/multi-thread-downloader/raw/master/preview/preview1.gif"/>


<img src="https://gitee.com/openneusoft/multi-thread-downloader/raw/master/preview/preview2.gif"/>

#### 安装教程

方法1.

1. 编译har包downloader.har。
2. 启动 DevEco Studio，将编译的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```

4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方法2.

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址

```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```

2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
dependencies {
    implementation 'io.github.dzsf:multi-thread-downloader:1.0.1'
}
```

#### 使用说明

1. 设置最大并行线程数

```
    DLManager.getInstance(MainAbility.this).setMaxTask(2);
```

 

2. 调用DLManager.getInstance(MainAbility.this).dlStart(String url, String dir, String name, List<DLHeader> headers, IDListener listener)方法即可实现下载
```
    btnStarts[i].setClickedListener(v -> {
    	textStatus[finalI].setText("Download starts");
    	DLManager.getInstance(MainAbility.this).dlStart(URLS[finalI], saveDir,
    			null, null, new SimpleDListener() {
    				private String fileSize;
    				private int fileLength;
    
    				private DecimalFormat df = new DecimalFormat("#.##");
    
    				@Override
    				public void onStart(String fileName, String realUrl, int fileLength) {
    					fileSize = TextUtils.converFileSize(fileLength);
    					this.fileLength = fileLength;
    
    					MainAbility.this.getUITaskDispatcher()
    							.delayDispatch(
    									new Runnable() {
    										@Override
    										public void run() {
    											pbDLs[finalI].setMinValue(0);
    											pbDLs[finalI].setMaxValue(fileLength);
    
    											textStatus[finalI].setText("Download started, file size " + fileSize);
    										}
    									},
    									0);
    				}
    
    				@Override
    				public void onProgress(int progress) {
    					StringBuilder stringBuilder = new StringBuilder();
    					stringBuilder.append("File download, file size ");
    					stringBuilder.append(fileSize);
    					stringBuilder.append(", current download ");
    					stringBuilder.append(df.format((double) progress / fileLength * 100));
    					stringBuilder.append("%");
    
    					MainAbility.this.getUITaskDispatcher()
    							.delayDispatch(
    									new Runnable() {
    										@Override
    										public void run() {
    											pbDLs[finalI].setProgressValue(progress);
    											textStatus[finalI].setText(stringBuilder.toString());
    										}
    									},
    									0);
    				}
    
    				@Override
    				public void onFinish(File file) {
    					MainAbility.this.getUITaskDispatcher()
    							.delayDispatch(
    									new Runnable() {
    										@Override
    										public void run() {
    											textStatus[finalI].setText("File download complete. File Path: " + file.getAbsolutePath());
    										}
    									},
    									0);
    				}
    
    				@Override
    				public void onError(int status, String error) {
    					MainAbility.this.getUITaskDispatcher()
    							.delayDispatch(
    									new Runnable() {
    										@Override
    										public void run() {
    											textStatus[finalI].setText(error);
    										}
    									},
    									0);
    				}
    
    				@Override
    				public void onStop(int progress) {
    					MainAbility.this.getUITaskDispatcher()
    							.delayDispatch(
    									new Runnable() {
    										@Override
    										public void run() {
    											textStatus[finalI].setText("File download stopped.");
    										}
    									},
    									0);
    				}
    
    				@Override
    				public void onPrepare() {
    					MainAbility.this.getUITaskDispatcher()
    							.delayDispatch(
    									new Runnable() {
    										@Override
    										public void run() {
    											textStatus[finalI].setText("Reading download file information.");
    										}
    									},
    									0);
    				}
    
    				@Override
    				public void onWait(int taskSize) {
    					MainAbility.this.getUITaskDispatcher()
    							.delayDispatch(
    									new Runnable() {
    										@Override
    										public void run() {
    											textStatus[finalI].setText("The current number of tasks exceeds the maximum number, " +
    													"waiting for other tasks to complete. " +
    													"The number of tasks currently executed is "
    													+ taskSize + ".");
    										}
    									},
    									0);
    				}
    			});
    });
```



3. 重写onStart方法可实现在下载开始后执行该方法

```
    @Override
    public void onStart(String fileName, String realUrl, int fileLength) {
    	fileSize = TextUtils.converFileSize(fileLength);
    	this.fileLength = fileLength;
    
    	MainAbility.this.getUITaskDispatcher()
    			.delayDispatch(
    					new Runnable() {
    						@Override
    						public void run() {
    							pbDLs[finalI].setMinValue(0);
    							pbDLs[finalI].setMaxValue(fileLength);
    
    							textStatus[finalI].setText("Download started, file size " + fileSize);
    						}
    					},
    					0);
    }
```



4. 重写onProgress方法可实现在下载过程中后执行该方法

```
    @Override
    public void onProgress(int progress) {
    	StringBuilder stringBuilder = new StringBuilder();
    	stringBuilder.append("File download, file size ");
    	stringBuilder.append(fileSize);
    	stringBuilder.append(", current download ");
    	stringBuilder.append(df.format((double) progress / fileLength * 100));
    	stringBuilder.append("%");
    
    	MainAbility.this.getUITaskDispatcher()
    			.delayDispatch(
    					new Runnable() {
    						@Override
    						public void run() {
    							pbDLs[finalI].setProgressValue(progress);
    							textStatus[finalI].setText(stringBuilder.toString());
    						}
    					},
    					0);
    }
```



5. 重写onFinish方法可实现在下载完成后执行该方法

```
    @Override
    public void onFinish(File file) {
        MainAbility.this.getUITaskDispatcher()
                .delayDispatch(
                        new Runnable() {
                            @Override
                            public void run() {
                                textStatus[finalI].setText("File download complete. File Path: " + file.getAbsolutePath());
                            }
                        },
                        0);
    }
```



6. 重写onError方法可实现在下载出错后执行该方法

```
   @Override
   public void onError(int status, String error) {
   	MainAbility.this.getUITaskDispatcher()
   			.delayDispatch(
   					new Runnable() {
   						@Override
   						public void run() {
   							textStatus[finalI].setText(error);
   						}
   					},
   					0);
   }
```



7. 重写onStop方法可实现在下载停止后执行该方法

```
   @Override
   public void onStop(int progress) {
   	MainAbility.this.getUITaskDispatcher()
   			.delayDispatch(
   					new Runnable() {
   						@Override
   						public void run() {
   							textStatus[finalI].setText("File download stopped.");
   						}
   					},
   					0);
   }
```



8. 重写onPrepare方法可实现在读取下载文件信息时执行该方法

```
   @Override
   public void onPrepare() {
   	MainAbility.this.getUITaskDispatcher()
   			.delayDispatch(
   					new Runnable() {
   						@Override
   						public void run() {
   							textStatus[finalI].setText("Reading download file information.");
   						}
   					},
   					0);
   }
```



9. 重写onWait方法可实现在新增下载任务没有空闲线程时调用该方法

```
   @Override
   public void onWait(int taskSize) {
   	MainAbility.this.getUITaskDispatcher()
   			.delayDispatch(
   					new Runnable() {
   						@Override
   						public void run() {
   							textStatus[finalI].setText("The current number of tasks exceeds the maximum number, " +
   									"waiting for other tasks to complete. " +
   									"The number of tasks currently executed is "
   									+ taskSize + ".");
   						}
   					},
   					0);
   }
```



#### 版本迭代

- v1.0.1

#### 版权和许可信息

```
Copyright 2014-2015 [AigeStudio](https://github.com/AigeStudio), [zhangchi](https://github.com/kxdd2002)

Licensed under the Apache License, Version 2.0 (the "License");you may not use this file except in compliance with the License.

You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
```