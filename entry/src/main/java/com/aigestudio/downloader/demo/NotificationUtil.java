package com.aigestudio.downloader.demo;

import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.app.Context;

/**
 * 通知工具类
 *
 * @author AigeStudio 2015-05-18
 */
public final class NotificationUtil {
    private static int id = 1;

    public static void notificationForDL(Context context, String url) {
        notificationForDL(context, url, context.getExternalCacheDir().getAbsolutePath() + "/AigeStudio/");
    }

    public synchronized static void notificationForDL(Context context, String url, String path) {
        Intent intent = new Intent();
        intent.setParam("url", url);
        intent.setParam("path", path);
        intent.setParam("id", id++);
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName("com.aigestudio.downloader.demo")
                .withAbilityName("com.aigestudio.downloader.demo.DLService")
                .build();
        intent.setOperation(operation);
        context.startAbility(intent, 1);
    }
}
