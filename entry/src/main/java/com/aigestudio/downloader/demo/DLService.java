package com.aigestudio.downloader.demo;

import com.aigestudio.downloader.bizs.DLManager;
import com.aigestudio.downloader.bizs.TextUtils;
import com.aigestudio.downloader.interfaces.SimpleDListener;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.event.notification.NotificationHelper;
import ohos.event.notification.NotificationRequest;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;
import ohos.rpc.IRemoteObject;
import ohos.rpc.RemoteException;

import java.io.File;
import java.io.InputStream;

public class DLService extends Ability {
    @Override
    protected void onCommand(Intent intent, boolean restart, int startId) {
        super.onCommand(intent, restart, startId);
        String url = intent.getStringParam("url");
        String path = intent.getStringParam("path");
        final int id = intent.getIntParam("id", -1);

        final NotificationRequest builder = new NotificationRequest(id);
        builder.setLittleIcon(getPixelMap(ResourceTable.Media_icon));

        final int[] length = new int[1];
        DLManager.getInstance(this).dlStart(url, path, null, null, new SimpleDListener() {

            @Override
            public void onStart(String fileName, String realUrl, int fileLength) {
                NotificationRequest.NotificationLongTextContent content = new NotificationRequest.NotificationLongTextContent();
                content.setTitle("File downloading, file size: " + TextUtils.converFileSize(fileLength));
                content.setLongText("Download file name: " + fileName);
                NotificationRequest.NotificationContent notificationContent = new NotificationRequest.NotificationContent(content);
                builder.setContent(notificationContent);
                length[0] = fileLength;
            }

            @Override
            public void onProgress(int progress) {
                builder.setProgressBar(progress, length[0], false);
                try {
                    NotificationHelper.publishNotification(builder);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFinish(File file) {
                try {
                    NotificationHelper.cancelNotification(id);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected IRemoteObject onConnect(Intent intent) {
        return null;
    }

    private PixelMap getPixelMap(int resId) {
        InputStream drawableInputStream = null;
        try {
            drawableInputStream = getResourceManager().getResource(resId);
            ImageSource.SourceOptions sourceOptions = new ImageSource.SourceOptions();
            sourceOptions.formatHint = "image/png";
            ImageSource imageSource = ImageSource.create(drawableInputStream, null);
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            decodingOptions.desiredSize = new Size(0, 0);
            decodingOptions.desiredRegion = new Rect(0, 0, 0, 0);
            decodingOptions.desiredPixelFormat = PixelFormat.ARGB_8888;
            PixelMap pixelMap = imageSource.createPixelmap(decodingOptions);
            return pixelMap;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (drawableInputStream != null) {
                    drawableInputStream.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
