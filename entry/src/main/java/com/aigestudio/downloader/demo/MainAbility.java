package com.aigestudio.downloader.demo;

import com.aigestudio.downloader.bizs.DLManager;
import com.aigestudio.downloader.bizs.TextUtils;
import com.aigestudio.downloader.interfaces.SimpleDListener;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.ProgressBar;
import ohos.agp.components.Text;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.io.File;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Random;

public class MainAbility extends Ability {
    private static final String TAG = MainAbility.class.getSimpleName();
    private static int DOMAIN = 0x0;
    private static HiLogLabel hiLogLabel = new HiLogLabel(HiLog.INFO, DOMAIN, TAG);

    private static final String[] URLS = {
            "https://down.qq.com/qqweb/PCQQ/PCQQ_EXE/PCQQ2021.exe",
            "http://yapkwww.cdn.anzhi.com/data5/apk/202007/23/f0afe070597c758e2e5ffea2a61cfd59_63952600.apk",
            "http://yapkwww.cdn.anzhi.com/data5/apk/202006/11/94385bee070f4aed0405cefabdc33ecb_97451200.apk",
            "http://yapkwww.cdn.anzhi.com/data5/apk/202012/08/30e0d8df6c7bbe6c5fd1530535a0d638_50859300.apk",
            "http://yapkwww.cdn.anzhi.com/data5/apk/202101/21/69209770aca5d5f5b9c6773ae3711f3f_19871000.apk",
            "http://yapkwww.cdn.anzhi.com/data5/apk/202104/02/ac7edc172099a8b54316532db7be5462_46739400.apk",
    };

    private static final int[] RES_ID_BTN_START = {
            ResourceTable.Id_main_dl_start_btn1,
            ResourceTable.Id_main_dl_start_btn2,
            ResourceTable.Id_main_dl_start_btn3,
            ResourceTable.Id_main_dl_start_btn4,
            ResourceTable.Id_main_dl_start_btn5,
            ResourceTable.Id_main_dl_start_btn6};
    private static final int[] RES_ID_BTN_STOP = {
            ResourceTable.Id_main_dl_stop_btn1,
            ResourceTable.Id_main_dl_stop_btn2,
            ResourceTable.Id_main_dl_stop_btn3,
            ResourceTable.Id_main_dl_stop_btn4,
            ResourceTable.Id_main_dl_stop_btn5,
            ResourceTable.Id_main_dl_stop_btn6};
    private static final int[] RES_ID_PB = {
            ResourceTable.Id_main_dl_pb1,
            ResourceTable.Id_main_dl_pb2,
            ResourceTable.Id_main_dl_pb3,
            ResourceTable.Id_main_dl_pb4,
            ResourceTable.Id_main_dl_pb5,
            ResourceTable.Id_main_dl_pb6};
    private static final int[] RES_ID_NOTIFY = {
            ResourceTable.Id_main_notify_btn1,
            ResourceTable.Id_main_notify_btn2,
            ResourceTable.Id_main_notify_btn3,
            ResourceTable.Id_main_notify_btn4,
            ResourceTable.Id_main_notify_btn5,
            ResourceTable.Id_main_notify_btn6};
    private static final int[] RES_ID_TEXT_STATUS = {
            ResourceTable.Id_main_dl_status_text1,
            ResourceTable.Id_main_dl_status_text2,
            ResourceTable.Id_main_dl_status_text3,
            ResourceTable.Id_main_dl_status_text4,
            ResourceTable.Id_main_dl_status_text5,
            ResourceTable.Id_main_dl_status_text6};

    private String saveDir;

    private volatile ProgressBar[] pbDLs;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        HiLog.debug(hiLogLabel, "Resume task from memory.");

        Text[] textStatus = new Text[RES_ID_TEXT_STATUS.length];
        for (int i = 0; i < textStatus.length; i++) {
            textStatus[i] = (Text) findComponentById(RES_ID_TEXT_STATUS[i]);
        }

        DLManager.getInstance(MainAbility.this).setMaxTask(2);
        Button[] btnStarts = new Button[RES_ID_BTN_START.length];
        for (int i = 0; i < btnStarts.length; i++) {
            btnStarts[i] = (Button) findComponentById(RES_ID_BTN_START[i]);
            final int finalI = i;
            btnStarts[i].setClickedListener(v -> {
                textStatus[finalI].setText("Download starts");
                DLManager.getInstance(MainAbility.this).dlStart(URLS[finalI], saveDir,
                        null, null, new SimpleDListener() {
                            private String fileSize;
                            private int fileLength;

                            private DecimalFormat df = new DecimalFormat("#.##");

                            @Override
                            public void onStart(String fileName, String realUrl, int fileLength) {
                                fileSize = TextUtils.converFileSize(fileLength);
                                this.fileLength = fileLength;

                                MainAbility.this.getUITaskDispatcher()
                                        .delayDispatch(
                                                new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        pbDLs[finalI].setMinValue(0);
                                                        pbDLs[finalI].setMaxValue(fileLength);

                                                        textStatus[finalI].setText("Download started, file size " + fileSize);
                                                    }
                                                },
                                                0);
                            }

                            @Override
                            public void onProgress(int progress) {
                                StringBuilder stringBuilder = new StringBuilder();
                                stringBuilder.append("File download, file size ");
                                stringBuilder.append(fileSize);
                                stringBuilder.append(", current download ");
                                stringBuilder.append(df.format((double) progress / fileLength * 100));
                                stringBuilder.append("%");

                                MainAbility.this.getUITaskDispatcher()
                                        .delayDispatch(
                                                new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        pbDLs[finalI].setProgressValue(progress);
                                                        textStatus[finalI].setText(stringBuilder.toString());
                                                    }
                                                },
                                                0);
                            }

                            @Override
                            public void onFinish(File file) {
                                MainAbility.this.getUITaskDispatcher()
                                        .delayDispatch(
                                                new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        textStatus[finalI].setText("File download complete. File Path: " + file.getAbsolutePath());
                                                    }
                                                },
                                                0);
                            }

                            @Override
                            public void onError(int status, String error) {
                                MainAbility.this.getUITaskDispatcher()
                                        .delayDispatch(
                                                new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        textStatus[finalI].setText(error);
                                                    }
                                                },
                                                0);
                            }

                            @Override
                            public void onStop(int progress) {
                                MainAbility.this.getUITaskDispatcher()
                                        .delayDispatch(
                                                new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        textStatus[finalI].setText("File download stopped.");
                                                    }
                                                },
                                                0);
                            }

                            @Override
                            public void onPrepare() {
                                MainAbility.this.getUITaskDispatcher()
                                        .delayDispatch(
                                                new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        textStatus[finalI].setText("Reading download file information.");
                                                    }
                                                },
                                                0);
                            }

                            @Override
                            public void onWait(int taskSize) {
                                MainAbility.this.getUITaskDispatcher()
                                        .delayDispatch(
                                                new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        textStatus[finalI].setText("The current number of tasks exceeds the maximum number, " +
                                                                "waiting for other tasks to complete. " +
                                                                "The number of tasks currently executed is "
                                                                + taskSize + ".");
                                                    }
                                                },
                                                0);
                            }
                        });
            });
        }

        Button[] btnStops = new Button[RES_ID_BTN_STOP.length];
        for (int i = 0; i < btnStops.length; i++) {
            btnStops[i] = (Button) findComponentById(RES_ID_BTN_STOP[i]);
            final int finalI = i;
            btnStops[i].setClickedListener(v -> DLManager.getInstance(MainAbility.this).dlStop(URLS[finalI]));
        }

        pbDLs = new ProgressBar[RES_ID_PB.length];
        for (int i = 0; i < pbDLs.length; i++) {
            pbDLs[i] = (ProgressBar) findComponentById(RES_ID_PB[i]);
            pbDLs[i].setMaxValue(100);
        }

        Button[] btnNotifys = new Button[RES_ID_NOTIFY.length];
        for (int i = 0; i < btnNotifys.length; i++) {
            btnNotifys[i] = (Button) findComponentById(RES_ID_NOTIFY[i]);
            final int finalI = i;
            btnNotifys[i].setClickedListener(component -> NotificationUtil.notificationForDL(MainAbility.this, URLS[finalI]));
        }

        saveDir = this.getExternalCacheDir().getAbsolutePath() + "/AigeStudio/";
    }

    @Override
    protected void onStop() {
        for (String url : URLS) {
            DLManager.getInstance(this).dlStop(url);
        }
        super.onStop();
    }
}
