package com.aigestudio.downloader.bizs;

import com.aigestudio.downloader.interfaces.SimpleDListener;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import org.junit.Test;

import java.io.File;
import java.lang.reflect.Field;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class MdownloaderOhosTest {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "MdownloaderOhosTest");

    @Test
    public void testSetMaxTask() {
        IAbilityDelegator iAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        Context context = iAbilityDelegator.getAppContext();
        DLManager dlm = DLManager.getInstance(context).setMaxTask(2);
        try {
            Field field = dlm.getClass().getDeclaredField("maxTask");
            field.setAccessible(true);
            int maxValue = field.getInt(dlm);
            assertEquals(2, maxValue);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
            fail("Exception");
        }
    }

    @Test
    public void testDonloadStart() {
        IAbilityDelegator iAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        Context context = iAbilityDelegator.getAppContext();
        String url = "https://down.qq.com/qqweb/PCQQ/PCQQ_EXE/PCQQ2021.exe";
        String path = context.getExternalCacheDir() + "/AigeStudio/";
        final File[] actFile = new File[1];
        DLManager dLManager = DLManager.getInstance(context);
        dLManager.dlStart(url, path, "PCQQ2021.exe", null, new SimpleDListener() {
            public void onStart(String fileName, String realUrl, int fileLength) {
                DLInfo dLInfo = dLManager.getDLDBManager().queryTaskInfo(url);
                assertEquals(true,dLInfo != null);
            }

            public void onFinish(File file) {
                actFile[0] = file;
            }
        });

        try {
            Thread.sleep(40000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertEquals(path + "PCQQ2021.exe" ,actFile[0].getAbsolutePath());
        assertEquals((actFile[0].length() > 0), true);
    }

    @Test
    public void testDonloadStart2() {
        IAbilityDelegator iAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        Context context = iAbilityDelegator.getAppContext();
        String url = "https://down.qq.com/qqweb/PCQQ/PCQQ_EXE/PCQQ2021.exe";
        String path = context.getExternalCacheDir() + "/AigeStudio/";
        final File[] actFile = new File[1];
        DLManager dLManager = DLManager.getInstance(context);
        dLManager.dlStart(url, path, "PCQQ2021.exe",  new SimpleDListener() {
            public void onStart(String fileName, String realUrl, int fileLength) {
                DLInfo dLInfo = dLManager.getDLDBManager().queryTaskInfo(url);
                assertEquals(true,dLInfo != null);
            }

            public void onFinish(File file) {
                actFile[0] = file;
            }
        });

        try {
            Thread.sleep(40000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertEquals(path + "PCQQ2021.exe" ,actFile[0].getAbsolutePath());
        assertEquals((actFile[0].length() > 0), true);
    }

    @Test
    public void testDonloadStart3() {
        IAbilityDelegator iAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        Context context = iAbilityDelegator.getAppContext();
        String url = "https://down.qq.com/qqweb/PCQQ/PCQQ_EXE/PCQQ2021.exe";
        String path = context.getExternalCacheDir() + "/AigeStudio/";
        final File[] actFile = new File[1];
        DLManager dLManager = DLManager.getInstance(context);
        dLManager.dlStart(url, path, new SimpleDListener() {
            public void onStart(String fileName, String realUrl, int fileLength) {
                DLInfo dLInfo = dLManager.getDLDBManager().queryTaskInfo(url);
                assertEquals(true,dLInfo != null);
            }

            public void onFinish(File file) {
                actFile[0] = file;
            }
        });

        try {
            Thread.sleep(40000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertEquals((actFile[0].length() > 0), true);
    }

    @Test
    public void testDonloadStart4() {
        IAbilityDelegator iAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        Context context = iAbilityDelegator.getAppContext();
        String url = "https://down.qq.com/qqweb/PCQQ/PCQQ_EXE/PCQQ2021.exe";
        String path = context.getExternalCacheDir() + "/AigeStudio/";
        final File[] actFile = new File[1];
        DLManager dLManager = DLManager.getInstance(context);
        dLManager.dlStart(url, new SimpleDListener() {
            public void onStart(String fileName, String realUrl, int fileLength) {
                DLInfo dLInfo = dLManager.getDLDBManager().queryTaskInfo(url);
                assertEquals(true,dLInfo != null);
            }

            public void onFinish(File file) {
                actFile[0] = file;
            }
        });

        try {
            Thread.sleep(40000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertEquals((actFile[0].length() > 0), true);
    }

    @Test
    public void testDonloadStart5() {
        IAbilityDelegator iAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        Context context = iAbilityDelegator.getAppContext();
        String url1 = "https://down.qq.com/qqweb/PCQQ/PCQQ_EXE/PCQQ2021.exe";
        String path1 = context.getExternalCacheDir() + "/AigeStudio/";
        final File[] actFile = new File[2];
        DLManager dLManager = DLManager.getInstance(context);
        dLManager.dlStart(url1, path1, "PCQQ2021.exe", null, new SimpleDListener() {
            public void onStart(String fileName, String realUrl, int fileLength) {
                DLInfo dLInfo = dLManager.getDLDBManager().queryTaskInfo(url1);
                assertEquals(true,dLInfo != null);
            }

            public void onFinish(File file) {
                actFile[0] = file;
            }
        });

        String url2 = "http://yapkwww.cdn.anzhi.com/data5/apk/202007/23/f0afe070597c758e2e5ffea2a61cfd59_63952600.apk";
        String path2 = context.getExternalCacheDir() + "/AigeStudio/";
        DLManager dLManager2 = DLManager.getInstance(context);
        dLManager2.dlStart(url2, path2, "f0afe070597c758e2e5ffea2a61cfd59_63952600.apk", null, new SimpleDListener() {
            public void onStart(String fileName, String realUrl, int fileLength) {
                DLInfo dLInfo = dLManager.getDLDBManager().queryTaskInfo(url1);
                assertEquals(true,dLInfo != null);
            }

            public void onFinish(File file) {
                actFile[1] = file;
            }
        });

        try {
            Thread.sleep(90000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertEquals(path1 + "PCQQ2021.exe" ,actFile[0].getAbsolutePath());
        assertEquals((actFile[0].length() > 0), true);
        assertEquals(path2 + "f0afe070597c758e2e5ffea2a61cfd59_63952600.apk" ,actFile[1].getAbsolutePath());
        assertEquals((actFile[1].length() > 0), true);
    }

    @Test
    public void testDonloadStop() {
        IAbilityDelegator iAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        Context context = iAbilityDelegator.getAppContext();
        String url = "https://down.qq.com/qqweb/PCQQ/PCQQ_EXE/PCQQ2021.exe";
        String path = context.getExternalCacheDir() + "/AigeStudio/";
        final String[] extfileName = {null};
        DLManager dLManager = DLManager.getInstance(context);
        dLManager.dlStart(url, path, "PCQQ2021.exe", null, new SimpleDListener() {
            @Override
            public void onStart(String fileName, String realUrl, int fileLength) {
                extfileName[0] = fileName;
            }
        });

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        dLManager.dlStop(url);
        assertEquals("PCQQ2021.exe", extfileName[0]);
        assertEquals(true, (new File(path + "PCQQ2021.exe").length() > 0));
        DLInfo dLInfo = dLManager.getDLDBManager().queryTaskInfo(url);
        assertEquals(true,dLInfo != null);

        Field field = null;
        try {
            field = dLManager.getClass().getDeclaredField("TASK_DLING");
            field.setAccessible(true);
            ConcurrentHashMap<String, DLInfo> TASK_DLING = (ConcurrentHashMap<String, DLInfo>)field.get(dLManager);
            DLInfo info = TASK_DLING.get(url);

            assertEquals(true, info.isStop);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testDonloadStopAndRestart() {
        IAbilityDelegator iAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        Context context = iAbilityDelegator.getAppContext();
        String url = "https://down.qq.com/qqweb/PCQQ/PCQQ_EXE/PCQQ2021.exe";
        String path = context.getExternalCacheDir() + "/AigeStudio/";
        final String[] extfileName = {null};
        DLManager dLManager = DLManager.getInstance(context);
        dLManager.dlStart(url, path, "PCQQ2021.exe", null, new SimpleDListener() {
            @Override
            public void onStart(String fileName, String realUrl, int fileLength) {
                extfileName[0] = fileName;
            }
        });

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        dLManager.dlStop(url);
        assertEquals("PCQQ2021.exe", extfileName[0]);
        long stopSize = new File(path + "PCQQ2021.exe").length();
        assertEquals(true, stopSize > 0);
        DLInfo dLInfo = dLManager.getDLDBManager().queryTaskInfo(url);
        assertEquals(true,dLInfo != null);

        Field field = null;
        try {
            field = dLManager.getClass().getDeclaredField("TASK_DLING");
            field.setAccessible(true);
            ConcurrentHashMap<String, DLInfo> TASK_DLING = (ConcurrentHashMap<String, DLInfo>)field.get(dLManager);
            DLInfo info = TASK_DLING.get(url);

            assertEquals(true, info.isStop);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // 继续下载
        final File[] actFile = new File[1];
        dLManager.dlStart(url, path, "PCQQ2021.exe", null, new SimpleDListener() {
            @Override
            public void onStart(String fileName, String realUrl, int fileLength) {
                extfileName[0] = fileName;
            }
            public void onFinish(File file) {
                actFile[0] = file;
            }
        });

        try {
            Thread.sleep(50000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertEquals(path + "PCQQ2021.exe" ,actFile[0].getAbsolutePath());
        assertEquals(true, (actFile[0].length() > 0));
        assertEquals(true, (actFile[0].length() > stopSize));
    }

    @Test
    public void testonCancel() {
        IAbilityDelegator iAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        Context context = iAbilityDelegator.getAppContext();
        String url = "https://down.qq.com/qqweb/PCQQ/PCQQ_EXE/PCQQ2021.exe";
        String path = context.getExternalCacheDir() + "/AigeStudio/";
        final String[] extfileName = {null};
        DLManager dLManager = DLManager.getInstance(context);
        dLManager.dlStart(url, path, "PCQQ2021.exe", null, new SimpleDListener() {
            @Override
            public void onStart(String fileName, String realUrl, int fileLength) {
                extfileName[0] = fileName;
            }
        });

        try {
            Thread.sleep(7000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertEquals(true, (new File(path + "PCQQ2021.exe").exists()));

        dLManager.dlCancel(url);

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertEquals("PCQQ2021.exe", extfileName[0]);
        assertEquals(false, (new File(path + "PCQQ2021.exe").exists()));
        DLInfo dLInfo = dLManager.getDLDBManager().queryTaskInfo(url);
        assertEquals(dLInfo == null, true);
    }

    @Test
    public void testDownloadExecption() {
        IAbilityDelegator iAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        Context context = iAbilityDelegator.getAppContext();
        String url = "https://down.qq.com/qqweb/PCQQ/PCQQ_EXE/XXXX.exe";
        String path = context.getExternalCacheDir() + "/AigeStudio/";
        final int[] errStatus = new int[1];
        DLManager dLManager = DLManager.getInstance(context);
        dLManager.dlStart(url, path, "PCQQ2021.exe", null, new SimpleDListener() {
            public void onStart(String fileName, String realUrl, int fileLength) {
                DLInfo dLInfo = dLManager.getDLDBManager().queryTaskInfo(url);
                assertEquals(true,dLInfo != null);
            }
            public void onError(int status, String error) {
                errStatus[0] = status;
            }
        });

        try {
            Thread.sleep(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertEquals(404, errStatus[0]);
    }
}